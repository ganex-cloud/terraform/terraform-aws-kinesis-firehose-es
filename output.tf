output "kinesis_firehose_stream_role_arn" {
  value       = join(",", aws_iam_role.kinesis_firehose_stream_role.*.arn)
  description = "The ARN of the IAM role to allow access to Elasticsearch cluster"
}

output "kinesis_firehose_stream_arn" {
  value       = aws_kinesis_firehose_delivery_stream.kinesis_firehose_stream.arn
  description = "The Amazon Resource Name (ARN) specifying the Stream"
}
