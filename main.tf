data "aws_caller_identity" "default" {}
data "aws_region" "default" {}

locals {
  s3_kms_key_arn = "${var.s3_kms_key_arn == "" ? "arn:aws:kms:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:key/NO-VALUE" : var.s3_kms_key_arn}"
}

resource "aws_kinesis_firehose_delivery_stream" "kinesis_firehose_stream" {
  name        = var.stream_name
  destination = "elasticsearch"
  tags        = var.tags

  server_side_encryption {
    enabled = var.stream_server_side_encryption
  }

  s3_configuration {
    role_arn           = aws_iam_role.kinesis_firehose_stream_role.arn
    bucket_arn         = var.s3_bucket_arn
    buffer_size        = var.s3_buffer_size
    buffer_interval    = var.s3_buffer_interval
    prefix             = var.s3_backup_prefix
    compression_format = var.s3_compression_format
    kms_key_arn        = var.s3_kms_key_arn

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
      log_stream_name = aws_cloudwatch_log_stream.kinesis_firehose_stream_logging_s3_stream.name
    }

  }

  elasticsearch_configuration {
    domain_arn            = var.stream_domain_arn
    role_arn              = aws_iam_role.kinesis_firehose_stream_role.arn
    index_name            = var.stream_index_name
    type_name             = var.stream_type_name
    buffering_interval    = var.stream_buffering_interval
    buffering_size        = var.stream_buffering_size
    retry_duration        = var.stream_retry_duration
    index_rotation_period = var.stream_index_rotation_period
    s3_backup_mode        = var.stream_s3_backup_mode

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
      log_stream_name = aws_cloudwatch_log_stream.kinesis_firehose_stream_logging_es_stream.name
    }

    dynamic "vpc_config" {
      for_each = var.vpc_config == null ? [] : [var.vpc_config]
      content {
        security_group_ids = vpc_config.value.security_group_ids
        subnet_ids         = vpc_config.value.subnet_ids
        role_arn           = aws_iam_role.kinesis_firehose_stream_role.arn
      }
    }
  }
}

data "aws_iam_policy_document" "kinesis_firehose_stream_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "kinesis_firehose_stream_policy" {
  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeVpcs",
      "ec2:DescribeVpcAttribute",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeNetworkInterfaces",
      "ec2:CreateNetworkInterface",
      "ec2:CreateNetworkInterfacePermission",
      "ec2:DeleteNetworkInterface"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject",
    ]

    resources = [
      "${var.s3_bucket_arn}",
      "${var.s3_bucket_arn}/*",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "kms:GenerateDataKey"
    ]

    resources = [
      "${local.s3_kms_key_arn}"
    ]

    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"
      values   = ["s3.${data.aws_region.default.name}.amazonaws.com"]
    }
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:s3:arn"
      values   = ["arn:aws:s3:::${var.s3_bucket_name}/${var.s3_backup_prefix}*"]
    }
  }

  statement {
    effect = "Allow"
    actions = [
      "es:DescribeElasticsearchDomain",
      "es:DescribeElasticsearchDomains",
      "es:DescribeElasticsearchDomainConfig",
      "es:ESHttpPost",
      "es:ESHttpPut"
    ]

    resources = [
      "${var.stream_domain_arn}",
      "${var.stream_domain_arn}/*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "es:ESHttpGet"
    ]

    resources = [
      "${var.stream_domain_arn}/_all/_settings",
      "${var.stream_domain_arn}/_cluster/stats",
      "${var.stream_domain_arn}/${var.stream_index_name}*/_mapping/${var.stream_type_name}",
      "${var.stream_domain_arn}/_nodes",
      "${var.stream_domain_arn}/_nodes/stats",
      "${var.stream_domain_arn}/_nodes/*/stats",
      "${var.stream_domain_arn}/_stats",
      "${var.stream_domain_arn}/${var.stream_index_name}*/_stats"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "kinesis:DescribeStream",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords",
      "kinesis:ListShards"
    ]

    resources = [
      "arn:aws:kinesis:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:stream/${var.stream_name}"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:log-group:${aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name}:log-stream:*",
    ]
  }
}

resource "aws_iam_role" "kinesis_firehose_stream_role" {
  name               = "${var.stream_name}-kinesis_firehose_stream_role"
  assume_role_policy = data.aws_iam_policy_document.kinesis_firehose_stream_assume_role.json
}

resource "aws_iam_role_policy" "kinesis_firehose_policy" {
  name   = "kinesis_firehose_policy"
  role   = aws_iam_role.kinesis_firehose_stream_role.name
  policy = data.aws_iam_policy_document.kinesis_firehose_stream_policy.json
}

resource "aws_cloudwatch_log_group" "kinesis_firehose_stream_logging_group" {
  name = "/aws/kinesisfirehose/${var.stream_name}"
  tags = var.tags
}

resource "aws_cloudwatch_log_stream" "kinesis_firehose_stream_logging_es_stream" {
  log_group_name = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
  name           = "ElasticsearchDelivery"
}

resource "aws_cloudwatch_log_stream" "kinesis_firehose_stream_logging_s3_stream" {
  log_group_name = aws_cloudwatch_log_group.kinesis_firehose_stream_logging_group.name
  name           = "S3Delivery"
}
