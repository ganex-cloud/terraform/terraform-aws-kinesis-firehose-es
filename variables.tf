variable "stream_name" {
  description = "Name to be use on kinesis firehose stream"
  type        = string
}

variable "stream_buffering_interval" {
  description = "(Optional) Buffer incoming data for the specified period of time, in seconds between 60 to 900, before delivering it to the destination. "
  type        = number
  default     = 300
}

variable "stream_buffering_size" {
  description = "(Optional) Buffer incoming data to the specified size, in MBs between 1 to 100, before delivering it to the destination. "
  type        = number
  default     = 5
}

variable "stream_retry_duration" {
  description = "(Optional) After an initial failure to deliver to Amazon Elasticsearch, the total amount of time, in seconds between 0 to 7200, during which Firehose re-attempts delivery (including the first attempt). After this time has elapsed, the failed documents are written to Amazon S3."
  type        = number
  default     = 300
}

variable "stream_domain_arn" {
  description = "(Required) The ARN of the Amazon ES domain. The IAM role must have permission for DescribeElasticsearchDomain, DescribeElasticsearchDomains, and DescribeElasticsearchDomainConfig after assuming RoleARN."
  type        = string
}

variable "stream_index_name" {
  description = "(Required) The Elasticsearch index name."
  type        = string
}

variable "stream_index_rotation_period" {
  description = "(Optional) The Elasticsearch index rotation period. Index rotation appends a timestamp to the IndexName to facilitate expiration of old data. Valid values are NoRotation, OneHour, OneDay, OneWeek, and OneMonth."
  type        = string
  default     = "OneDay"
}

variable "stream_s3_backup_mode" {
  description = "(Optional) Defines how documents should be delivered to Amazon S3. Valid values are FailedDocumentsOnly and AllDocuments"
  type        = string
  default     = "FailedDocumentsOnly"
}

variable "stream_server_side_encryption" {
  description = "(Optional) Encrypt at rest options. Server-side encryption should not be enabled when a kinesis stream is configured as the source of the firehose delivery stream."
  type        = bool
  default     = false
}

variable "s3_backup_prefix" {
  description = "The prefix name to use for the kinesis backup"
  type        = string
  default     = "backup/"
}

variable "stream_type_name" {
  description = "(Required) The Elasticsearch type name with maximum length of 100 characters."
  type        = string
}

variable "s3_bucket_name" {
  description = "(Required) The S3 bucket name"
  type        = string
}

variable "s3_bucket_arn" {
  description = "(Required) The ARN of the S3 bucket"
  type        = string
}

variable "s3_buffer_size" {
  description = "(Optional) Buffer incoming data to the specified size, in MBs, before delivering it to the destination."
  type        = number
  default     = 5
}

variable "s3_buffer_interval" {
  description = "(Optional) Buffer incoming data for the specified period of time, in seconds, before delivering it to the destination. "
  type        = number
  default     = 300
}

variable "s3_compression_format" {
  description = "(Optional) The compression format. If no value is specified, the default is UNCOMPRESSED."
  type        = string
  default     = "UNCOMPRESSED"
}

variable "s3_kms_key_arn" {
  description = "(Optional) Specifies the KMS key ARN the stream will use to encrypt data. If not set, no encryption will be used."
  type        = string
  default     = ""
}

variable "cloudwatch_logging_enabled" {
  description = "Enable cloudwatch logs"
  type        = bool
  default     = false
}

variable "vpc_config" {
  description = "(Optional) The VPC configuration for the delivery stream to connect to Elastic Search associated with the VPC"
  type = object({
    subnet_ids         = list(string)
    security_group_ids = list(string)
  })
  default = null
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}